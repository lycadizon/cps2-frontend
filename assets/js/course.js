let params = new URLSearchParams(window.location.search)
let token =localStorage.getItem("token");


let courseId = params.get('courseId')
let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");


fetch(`https://lyca-capstone-2.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;
	enrollContainer.innerHTML = 
	`
		<button id = "enrollButton" class="btn btn-block btn-primary">Enroll</button>
	`

	document.querySelector(`#enrollButton`).addEventListener("click", ()=> {
		// console.log("You enrolled!");

		fetch("https://lyca-capstone-2.herokuapp.com/api/users/enroll", {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId,
			}),
		})
			.then((res) => {
				return res.json()
			})
			.then((data) => {
				// console.log(data)
				if(data === true) {
					alert("Thank you for enrolling!")
					window.location.replace("./courses.html")
				}else {
					alert("Oops... Something went wrong!")
				}
			})
	})

})

