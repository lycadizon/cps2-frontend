//IMPORT
//TO LET THE PAGE KNOW IF USER IS REGULAR OR AN ADMIN
let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector("#adminButton")//we're accessing the DOM
let cardFooter;

//CONDITIONAL RENDERING TO SHOW AN ADD COURSE BUTTON ONLY IF ADMIN. IF NOT ADMIN, BUTTON WILL NOT PRESENT
if(adminUser === "false" || !adminUser){
	modalButton.innerHTML = null
}else{
	modalButton.innerHTML =
	`
		<div class="col-md-2 offset-md-10">
			<a href ="./addCourse.html" class="btn btn-block btn-primary"> Add Course </a>
		</div>  
	` 
}


//FETCH THE COURSES FROM OUR API 
fetch('https://lyca-capstone-2.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {
	let courseData;
	//IF NO COURSES, DISPLAY NO COURSES AVAILABLE
	if (data.length < 1){
		courseData = "No courses available"
	}//IF REGULAR USER, DISPLAY SELECT COURSE BUTTON ONLY
	else{
		courseData = data.map(course => {
			if(adminUser === "false" || !adminUser){
				cardFooter = 
				`
					<a href = "./course.html?courseId=${course._id}" class = "btn btn-primary text-white btn-block editButton">Select Course</a>
				`
			} 
			//IF ADMIN USER, DISPLAY ENROLEES, EDIT, ENABLE & DISABLE BUTTONS
			else{
				cardFooter =
				`
					<a href = "viewEnrollees.html?courseId=${course._id}" class = "btn btn-primary text-white btn-block viewButton">View All Enrollees</a>

					<a href = "editCourse.html?courseId=${course._id}" class = "btn btn-primary text-white btn-block editButton">Edit</a>

					<a href = "enableCourse.html?courseId=${course._id}" class = "btn btn-success text-white btn-block enableButton">Enable Course</a>

					<a href = "deleteCourse.html?courseId=${course._id}" class = "btn btn-danger text-white btn-block dangerButton">Disable Course</a>

				`
			}

			return(
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class = "card-body">
							<h5 class = "card-title">${course.name}</h5>
								<p class="card-text text-left">
									${course.description}
								</p>

								<p class="card-text text-right">
									${course.price}
								</p>
						</div>

						<div class = "card-footer">
							${cardFooter}
						</div>
					</div>
				</div>

				`
			)

		}).join("")
	}

	let container = document.querySelector("#coursesContainer")

	container.innerHTML = courseData
})
  

